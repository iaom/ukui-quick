/*
 * Copyright (C) 2024, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: amingamingaming <wangyiming01@kylinos.cn>
 *
 */

import QtQuick 2.15
import QtGraphicalEffects 1.15
import org.ukui.quick.items 1.0
import org.ukui.quick.platform 1.0

Rectangle {
    //是否跟随系统主题颜色的透明度
    property bool useStyleTransparency: true;
    //背景颜色
    property var backgroundColor : GlobalTheme.windowActive;
    property var borderColor :  GlobalTheme.baseActive
    property real borderAlpha: 1.0;
    property real alpha: 1.0
    border.width: 0;
    border.color : borderColor.setAlphaF(borderColor.pureColor,borderAlpha);
    color:backgroundColor.setAlphaF(backgroundColor.pureColor, useStyleTransparency ? GlobalTheme.transparency : alpha);
    clip: true;
    LinearGradient {
        id : linerGra
        anchors.fill: parent
        start: backgroundColor.getGradientStartPoint(backgroundColor.gradientDeg, width, height)
        end: backgroundColor.getGradientEndPoint(backgroundColor.gradientDeg, width, height)
        visible: backgroundColor.colorType === DtColorType.Gradient
        gradient : Gradient {
            GradientStop { position : 0.0; color: backgroundColor.mixBackGroundColor(backgroundColor.gradientStart, backgroundColor.gradientBackground, useStyleTransparency ? GlobalTheme.transparency : alpha)}
            GradientStop { position : 1.0; color: backgroundColor.mixBackGroundColor(backgroundColor.gradientEnd, backgroundColor.gradientBackground, useStyleTransparency ? GlobalTheme.transparency : alpha)}
        }
    }
}

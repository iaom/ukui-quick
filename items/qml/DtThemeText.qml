/*
 * Copyright (C) 2025, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: amingamingaming <wangyiming01@kylinos.cn>
 *
 */

import QtQuick 2.12
import org.ukui.quick.items 1.0
import org.ukui.quick.platform 1.0

Text {
    //DT主题字体颜色
    property var textColor: GlobalTheme.textActive;
    property int pointSizeOffset: 0
    property real alpha: 1.0;
    font {
        pointSize: {
            if ((GlobalTheme.fontSize + pointSizeOffset) < 0 ) {
                return GlobalTheme.fontSize;
            } else {
                return GlobalTheme.fontSize + pointSizeOffset;
            }
        }
        family: GlobalTheme.fontFamily
    }
    color: textColor.setAlphaF(textColor.pureColor, alpha)
}

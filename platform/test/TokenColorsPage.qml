import QtQuick 2.0
import org.ukui.quick.platform 1.0
import QtQuick.Layouts 1.15
GridView {
    id: activeView
    width: parent.width
    height: parent.height
    interactive: true

    cellWidth: 140
    model: tokenModel
    delegate: TokenColor {
        color: dtToken
        colorText: model.name
    }
    clip: true


    ListModel {
        id: tokenModel
        dynamicRoles: true

        Component.onCompleted: {
            append({"dtToken": GlobalTheme.windowTextActive, "name": "windowTextActive"})
            append({"dtToken": GlobalTheme.windowTextInactive, "name": "windowTextInactive"})
            append({"dtToken": GlobalTheme.windowTextDisable, "name": "windowTextDisable"})
            append({"dtToken": GlobalTheme.buttonActive, "name": "buttonActive"})
            append({"dtToken": GlobalTheme.buttonInactive, "name": "buttonInactive"})
            append({"dtToken": GlobalTheme.buttonDisable, "name": "buttonDisable"})
            append({"dtToken": GlobalTheme.lightActive, "name": "lightActive"})
            append({"dtToken": GlobalTheme.lightInactive, "name": "lightInactive"})
            append({"dtToken": GlobalTheme.lightDisable, "name": "lightDisable"})
            append({"dtToken": GlobalTheme.darkActive, "name": "darkActive"})
            append({"dtToken": GlobalTheme.darkInactive, "name": "darkInactive"})
            append({"dtToken": GlobalTheme.darkDisable, "name": "darkDisable"})
            append({"dtToken": GlobalTheme.baseActive, "name": "baseActive"})
            append({"dtToken": GlobalTheme.baseInactive, "name": "baseInactive"})
            append({"dtToken": GlobalTheme.baseDisable, "name": "baseDisable"})
            append({"dtToken": GlobalTheme.midLightActive, "name": "midLightActive"})
            append({"dtToken": GlobalTheme.midLightInactive, "name": "midLightInactive"})
            append({"dtToken": GlobalTheme.midLightDisable, "name": "midLightDisable"})
            append({"dtToken": GlobalTheme.midActive, "name": "midActive"})
            append({"dtToken": GlobalTheme.midInactive, "name": "midInactive"})
            append({"dtToken": GlobalTheme.midDisable, "name": "midDisable"})
            append({"dtToken": GlobalTheme.textActive, "name": "textActive"})
            append({"dtToken": GlobalTheme.textInactive, "name": "textInactive"})
            append({"dtToken": GlobalTheme.textDisable, "name": "textDisable"})
            append({"dtToken": GlobalTheme.brightTextActive, "name": "brightTextActive"})
            append({"dtToken": GlobalTheme.brightTextInactive, "name": "brightTextInactive"})
            append({"dtToken": GlobalTheme.brightTextDisable, "name": "brightTextDisable"})
            append({"dtToken": GlobalTheme.buttonTextActive, "name": "buttonTextActive"})
            append({"dtToken": GlobalTheme.buttonTextInactive, "name": "buttonTextInactive"})
            append({"dtToken": GlobalTheme.buttonTextDisable, "name": "buttonTextDisable"})
            append({"dtToken": GlobalTheme.baseActive, "name": "baseActive"})
            append({"dtToken": GlobalTheme.baseInactive, "name": "baseInactive"})
            append({"dtToken": GlobalTheme.baseDisable, "name": "baseDisable"})
            append({"dtToken": GlobalTheme.windowActive, "name": "windowActive"})
            append({"dtToken": GlobalTheme.windowInactive, "name": "windowInactive"})
            append({"dtToken": GlobalTheme.windowDisable, "name": "windowDisable"})
            append({"dtToken": GlobalTheme.shadowActive, "name": "shadowActive"})
            append({"dtToken": GlobalTheme.shadowInactive, "name": "shadowInactive"})
            append({"dtToken": GlobalTheme.shadowDisable, "name": "shadowDisable"})
            append({"dtToken": GlobalTheme.highlightActive, "name": "highlightActive"})
            append({"dtToken": GlobalTheme.highlightInactive, "name": "highlightInactive"})
            append({"dtToken": GlobalTheme.highlightDisable, "name": "highlightDisable"})
            append({"dtToken": GlobalTheme.linkActive, "name": "linkActive"})
            append({"dtToken": GlobalTheme.linkInactive, "name": "linkInactive"})
            append({"dtToken": GlobalTheme.linkDisable, "name": "linkDisable"})
            append({"dtToken": GlobalTheme.linkVisitedActive, "name": "linkVisitedActive"})
            append({"dtToken": GlobalTheme.linkVisitedInactive, "name": "linkVisitedInactive"})
            append({"dtToken": GlobalTheme.linkVisitedDisable, "name": "linkVisitedDisable"})
            append({"dtToken": GlobalTheme.alternateBaseActive, "name": "alternateBaseActive"})
            append({"dtToken": GlobalTheme.alternateBaseInactive, "name": "alternateBaseInactive"})
            append({"dtToken": GlobalTheme.alternateBaseDisable, "name": "alternateBaseDisable"})
            append({"dtToken": GlobalTheme.noRoleActive, "name": "noRoleActive"})
            append({"dtToken": GlobalTheme.noRoleInactive, "name": "noRoleInactive"})
            append({"dtToken": GlobalTheme.noRoleDisable, "name": "noRoleDisable"})
            append({"dtToken": GlobalTheme.toolTipBaseActive, "name": "toolTipBaseActive"})
            append({"dtToken": GlobalTheme.toolTipBaseInactive, "name": "toolTipBaseInactive"})
            append({"dtToken": GlobalTheme.toolTipBaseDisable, "name": "toolTipBaseDisable"})
            append({"dtToken": GlobalTheme.toolTipTextActive, "name": "toolTipTextActive"})
            append({"dtToken": GlobalTheme.toolTipTextInactive, "name": "toolTipTextInactive"})
            append({"dtToken": GlobalTheme.toolTipTextDisable, "name": "toolTipTextDisable"})
            append({"dtToken": GlobalTheme.placeholderTextActive, "name": "placeholderTextActive"})
            append({"dtToken": GlobalTheme.placeholderTextInactive, "name": "placeholderTextInactive"})
            append({"dtToken": GlobalTheme.placeholderTextDisable, "name": "placeholderTextDisable"})
            append({"dtToken": GlobalTheme.kWhite, "name": "kWhite"})
            append({"dtToken": GlobalTheme.kBlack, "name": "kBlack"})
            append({"dtToken": GlobalTheme.kGray0, "name": "kGray0"})
            append({"dtToken": GlobalTheme.kGray1, "name": "kGray1"})
            append({"dtToken": GlobalTheme.kGray2, "name": "kGray2"})
            append({"dtToken": GlobalTheme.kGray3, "name": "kGray3"})
            append({"dtToken": GlobalTheme.kGray4, "name": "kGray4"})
            append({"dtToken": GlobalTheme.kGray5, "name": "kGray5"})
            append({"dtToken": GlobalTheme.kGray6, "name": "kGray6"})
            append({"dtToken": GlobalTheme.kGray7, "name": "kGray7"})
            append({"dtToken": GlobalTheme.kGray8, "name": "kGray8"})
            append({"dtToken": GlobalTheme.kGray9, "name": "kGray9"})
            append({"dtToken": GlobalTheme.kGray10, "name": "kGray10"})
            append({"dtToken": GlobalTheme.kGray11, "name": "kGray11"})
            append({"dtToken": GlobalTheme.kGray12, "name": "kGray12"})
            append({"dtToken": GlobalTheme.kGray13, "name": "kGray13"})
            append({"dtToken": GlobalTheme.kGray14, "name": "kGray14"})
            append({"dtToken": GlobalTheme.kGray15, "name": "kGray15"})
            append({"dtToken": GlobalTheme.kGray16, "name": "kGray16"})
            append({"dtToken": GlobalTheme.kGray17, "name": "kGray17"})
            append({"dtToken": GlobalTheme.kGrayAlpha0, "name": "kGrayAlpha0"})
            append({"dtToken": GlobalTheme.kGrayAlpha1, "name": "kGrayAlpha1"})
            append({"dtToken": GlobalTheme.kGrayAlpha2, "name": "kGrayAlpha2"})
            append({"dtToken": GlobalTheme.kGrayAlpha3, "name": "kGrayAlpha3"})
            append({"dtToken": GlobalTheme.kGrayAlpha4, "name": "kGrayAlpha4"})
            append({"dtToken": GlobalTheme.kGrayAlpha5, "name": "kGrayAlpha5"})
            append({"dtToken": GlobalTheme.kGrayAlpha6, "name": "kGrayAlpha6"})
            append({"dtToken": GlobalTheme.kGrayAlpha7, "name": "kGrayAlpha7"})
            append({"dtToken": GlobalTheme.kGrayAlpha8, "name": "kGrayAlpha8"})
            append({"dtToken": GlobalTheme.kGrayAlpha9, "name": "kGrayAlpha9"})
            append({"dtToken": GlobalTheme.kGrayAlpha10, "name": "kGrayAlpha10"})
            append({"dtToken": GlobalTheme.kGrayAlpha11, "name": "kGrayAlpha11"})
            append({"dtToken": GlobalTheme.kGrayAlpha12, "name": "kGrayAlpha12"})
            append({"dtToken": GlobalTheme.kGrayAlpha13, "name": "kGrayAlpha13"})
            append({"dtToken": GlobalTheme.kFontStrong, "name": "kFontStrong"})
            append({"dtToken": GlobalTheme.kFontPrimary, "name": "kFontPrimary"})
            append({"dtToken": GlobalTheme.kFontPrimaryDisable, "name": "kFontPrimaryDisable"})
            append({"dtToken": GlobalTheme.kFontSecondary, "name": "kFontSecondary"})
            append({"dtToken": GlobalTheme.kFontSecondaryDisable, "name": "kFontSecondaryDisable"})
            append({"dtToken": GlobalTheme.kFontWhite, "name": "kFontWhite"})
            append({"dtToken": GlobalTheme.kFontWhiteDisable, "name": "kFontWhiteDisable"})
            append({"dtToken": GlobalTheme.kFontWhiteSecondary, "name": "kFontWhiteSecondary"})
            append({"dtToken": GlobalTheme.kFontWhiteSecondaryDisable, "name": "kFontWhiteSecondaryDisable"})
            append({"dtToken": GlobalTheme.kBrandNormal, "name": "kBrandNormal"})
            append({"dtToken": GlobalTheme.kBrandHover, "name": "kBrandHover"})
            append({"dtToken": GlobalTheme.kBrandClick, "name": "kBrandClick"})
            append({"dtToken": GlobalTheme.kBrandFocus, "name": "kBrandFocus"})
            append({"dtToken": GlobalTheme.kSuccessNormal, "name": "kSuccessNormal"})
            append({"dtToken": GlobalTheme.kSuccessHover, "name": "kSuccessHover"})
            append({"dtToken": GlobalTheme.kSuccessClick, "name": "kSuccessClick"})
            append({"dtToken": GlobalTheme.kWarningNormal, "name": "kWarningNormal"})
            append({"dtToken": GlobalTheme.kWarningHover, "name": "kWarningHover"})
            append({"dtToken": GlobalTheme.kWarningClick, "name": "kWarningClick"})
            append({"dtToken": GlobalTheme.kErrorNormal, "name": "kErrorNormal"})
            append({"dtToken": GlobalTheme.kErrorHover, "name": "kErrorHover"})
            append({"dtToken": GlobalTheme.kErrorClick, "name": "kErrorClick"})
            append({"dtToken": GlobalTheme.kBrand1, "name": "kBrand1"})
            append({"dtToken": GlobalTheme.kBrand2, "name": "kBrand2"})
            append({"dtToken": GlobalTheme.kBrand3, "name": "kBrand3"})
            append({"dtToken": GlobalTheme.kBrand4, "name": "kBrand4"})
            append({"dtToken": GlobalTheme.kBrand5, "name": "kBrand5"})
            append({"dtToken": GlobalTheme.kBrand6, "name": "kBrand6"})
            append({"dtToken": GlobalTheme.kBrand7, "name": "kBrand7"})
            append({"dtToken": GlobalTheme.kContainHover, "name": "kContainHover"})
            append({"dtToken": GlobalTheme.kContainClick, "name": "kContainClick"})
            append({"dtToken": GlobalTheme.kContainGeneralNormal, "name": "kContainGeneralNormal"})
            append({"dtToken": GlobalTheme.kContainSecondaryNormal, "name": "kContainSecondaryNormal"})
            append({"dtToken": GlobalTheme.kContainSecondaryAlphaNormal, "name": "kContainSecondaryAlphaNormal"})
            append({"dtToken": GlobalTheme.kContainSecondaryAlphaHover, "name": "kContainSecondaryAlphaHover"})
            append({"dtToken": GlobalTheme.kContainSecondaryAlphaClick, "name": "kContainSecondaryAlphaClick"})
            append({"dtToken": GlobalTheme.kComponentNormal, "name": "kComponentNormal"})
            append({"dtToken": GlobalTheme.kComponentHover, "name": "kComponentHover"})
            append({"dtToken": GlobalTheme.kComponentClick, "name": "kComponentClick"})
            append({"dtToken": GlobalTheme.kComponentDisable, "name": "kComponentDisable"})
            append({"dtToken": GlobalTheme.kComponentAlphaNormal, "name": "kComponentAlphaNormal"})
            append({"dtToken": GlobalTheme.kComponentAlphaHover, "name": "kComponentAlphaHover"})
            append({"dtToken": GlobalTheme.kComponentAlphaClick, "name": "kComponentAlphaClick"})
            append({"dtToken": GlobalTheme.kComponentAlphaDisable, "name": "kComponentAlphaDisable"})
            append({"dtToken": GlobalTheme.kLineWindow, "name": "kLineWindow"})
            append({"dtToken": GlobalTheme.kLineWindowActive, "name": "kLineWindowActive"})
            append({"dtToken": GlobalTheme.kLineNormalAlpha, "name": "kLineNormalAlpha"})
            append({"dtToken": GlobalTheme.kLineDisableAlpha, "name": "kLineDisableAlpha"})
            append({"dtToken": GlobalTheme.kLineComponentNormal, "name": "kLineComponentNormal"})
            append({"dtToken": GlobalTheme.kLineComponentHover, "name": "kLineComponentHover"})
            append({"dtToken": GlobalTheme.kLineComponentClick, "name": "kLineComponentClick"})
            append({"dtToken": GlobalTheme.kLineComponentDisable, "name": "kLineComponentDisable"})
            append({"dtToken": GlobalTheme.kLineBrandNormal, "name": "kLineBrandNormal"})
            append({"dtToken": GlobalTheme.kLineBrandHover, "name": "kLineBrandHover"})
            append({"dtToken": GlobalTheme.kLineBrandClick, "name": "kLineBrandClick"})
            append({"dtToken": GlobalTheme.kLineBrandDisable, "name": "kLineBrandDisable"})
            append({"dtToken": GlobalTheme.kModalMask, "name": "kModalMask"})
            append({"dtToken": GlobalTheme.kErrorDisable, "name": "kErrorDisable"})
            append({"dtToken": GlobalTheme.kWarningDisable, "name": "kWarningDisable"})
            append({"dtToken": GlobalTheme.kLineNormal, "name": "kLineNormal"})
            append({"dtToken": GlobalTheme.tokenColor, "name": "tokenColor"})
            append({"dtToken": GlobalTheme.kContainGeneralAlphaNormal, "name": "kContainGeneralAlphaNormal"})
            append({"dtToken": GlobalTheme.kSuccessDisable, "name": "kSuccessDisable"})
            append({"dtToken": GlobalTheme.kLineWindowInactive, "name": "kLineWindowInactive"})
            append({"dtToken": GlobalTheme.tokenColor1, "name": "tokenColor1"})
            append({"dtToken": GlobalTheme.kContainAlphaClick, "name": "kContainAlphaClick"})
            append({"dtToken": GlobalTheme.kContainAlphaHover, "name": "kContainAlphaHover"})
            append({"dtToken": GlobalTheme.kBrandDisable, "name": "kBrandDisable"})
            append({"dtToken": GlobalTheme.kLineDisable, "name": "kLineDisable"})
            append({"dtToken": GlobalTheme.kDivider, "name": "kDivider"})
            // append({"dtToken": GlobalTheme.normalLine, "name": "normalLine"})
            // append({"dtToken": GlobalTheme.focusLine, "name": "focusLine"})
            // append({"dtToken": GlobalTheme.kRadiusMin, "name": "kRadiusMin"})
            // append({"dtToken": GlobalTheme.kRadiusNormal, "name": "kRadiusNormal"})
            // append({"dtToken": GlobalTheme.kRadiusMenu, "name": "kRadiusMenu"})
            // append({"dtToken": GlobalTheme.kRadiusWindow, "name": "kRadiusWindow"})
            // append({"dtToken": GlobalTheme.kMarginMin, "name": "kMarginMin"})
            // append({"dtToken": GlobalTheme.kMarginNormal, "name": "kMarginNormal"})
            // append({"dtToken": GlobalTheme.kMarginBig, "name": "kMarginBig"})
            // append({"dtToken": GlobalTheme.kMarginWindow, "name": "kMarginWindow"})
            // append({"dtToken": GlobalTheme.kMarginComponent, "name": "kMarginComponent"})
            // append({"dtToken": GlobalTheme.kPaddingMinLeft, "name": "kPaddingMinLeft"})
            // append({"dtToken": GlobalTheme.kPaddingMinRight, "name": "kPaddingMinRight"})
            // append({"dtToken": GlobalTheme.kPaddingMinTop, "name": "kPaddingMinTop"})
            // append({"dtToken": GlobalTheme.kPaddingMinBottom, "name": "kPaddingMinBottom"})
            // append({"dtToken": GlobalTheme.kPaddingNormalLeft, "name": "kPaddingNormalLeft"})
            // append({"dtToken": GlobalTheme.kPaddingNormalRight, "name": "kPaddingNormalRight"})
            // append({"dtToken": GlobalTheme.kPaddingNormalTop, "name": "kPaddingNormalTop"})
            // append({"dtToken": GlobalTheme.kPaddingNormalBottom, "name": "kPaddingNormalBottom"})
            // append({"dtToken": GlobalTheme.kPadding8Left, "name": "kPadding8Left"})
            // append({"dtToken": GlobalTheme.kPadding8Right, "name": "kPadding8Right"})
            // append({"dtToken": GlobalTheme.kPadding8Top, "name": "kPadding8Top"})
            // append({"dtToken": GlobalTheme.kPadding8Bottom, "name": "kPadding8Bottom"})
            // append({"dtToken": GlobalTheme.kPaddingWindowLeft, "name": "kPaddingWindowLeft"})
            // append({"dtToken": GlobalTheme.kPaddingWindowRight, "name": "kPaddingWindowRight"})
            // append({"dtToken": GlobalTheme.kPaddingWindowTop, "name": "kPaddingWindowTop"})
            // append({"dtToken": GlobalTheme.kPaddingWindowBottom, "name": "kPaddingWindowBottom"})
        }
    }
}

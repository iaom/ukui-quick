import QtQuick 2.12
import org.ukui.quick.items 1.0
import org.ukui.quick.platform 1.0
Column {
    property alias color: backGround.backgroundColor
    property alias colorText: colorText.text
    width: childrenRect.width
    DtThemeBackground {
        id: backGround
        width: 120
        height: 80
        radius: 0
    }
    StyleText {
        id: colorText
        width: 140
        wrapMode: Text.WrapAnywhere
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
}

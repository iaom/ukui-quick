import QtQuick 2.15
import QtQuick.Controls 1.4

TabView {
    Tab {
        width: parent.width
        height: parent.height
        title: "Palette"
        PaletteColorPage {}
    }
    Tab {
        width: parent.width
        title: "Settings"
        SettingsValuePage {}
    }
    Tab {
        width: parent.width
        height: parent.height
        title: "AppLauncher"
        AppLauncherPage {}
    }
    Tab {
        width: parent.width
        height: parent.height
        title: "ToolTip"
        ToolTipTest {}
    }
    Tab {
        width: parent.width
        height: parent.height
        title: "Design Token"
        TokenColorsPage {}
    }
    Tab {
        width: parent.width
        height: parent.height
        title: "Design Token"
        TokenIntsPage {}
    }
}

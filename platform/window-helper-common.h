/*
* Copyright (C) 2024, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */

#ifndef WINDOW_HELPER_COMMON_H
#define WINDOW_HELPER_COMMON_H
#include <QMap>
#include "window-type.h"
#include "window-helper.h"
namespace UkuiQuick {
static QMap<WindowType::Type, QString> ukui_surface_roleMap = {
    {WindowType::Normal, "normal"},
    {WindowType::Desktop, "desktop"},
    {WindowType::Dock, "dock"},
    {WindowType::Panel, "panel"},
    {WindowType::OnScreenDisplay, "onscreendisplay"},
    {WindowType::Notification, "notification"},
    {WindowType::Menu, "menu"},
    {WindowType::PopupMenu, "popupmenu"},
    {WindowType::ToolTip, "tooltip"},
    {WindowType::CriticalNotification, "criticalnotification"},
    {WindowType::AppletPopup, "appletpopup"},
    {WindowType::ScreenLock, "screenlock"},
    {WindowType::WaterMark, "watermark"},
    {WindowType::SystemWindow,"systemwindow"},
    {WindowType::InputPanel,"inputpanel"},
    {WindowType::Logout,"logout"},
    {WindowType::ScreenLockNotification,"screenlocknotification"},
    {WindowType::Switcher,"switcher"}
};

static QMap<WindowProxy::SlideFromEdge, QString> ukui_surface_slideEdge = {
    {WindowProxy::NoEdge, "none"},
    {WindowProxy::TopEdge, "top"},
    {WindowProxy::BottomEdge, "bottom"},
    {WindowProxy::LeftEdge, "left"},
    {WindowProxy::RightEdge, "right"}
};
}
#endif //WINDOW_HELPER_COMMON_H

import QtQml 2.15
import QtQuick 2.15
import org.ukui.quick.platform 1.0

DtThemeDefinition {
    id: dtTheme
    //QPalette 颜色
    property color windowTextActive : kFontPrimary
    property color windowTextInactive : kFontSecondaryDisable
    property color windowTextDisable : kFontPrimaryDisable
    property color buttonActive : kGray6
    property color buttonInactive : kGray6
    property color buttonDisable : kGray4
    property color lightActive : kWhite
    property color lightInactive : kWhite
    property color lightDisable : kGray1
    property color midLightActive : kGray7
    property color midLightInactive : kGray7
    property color midLightDisable : kGray6
    property color darkActive : kBlack
    property color darkInactive : kBlack
    property color darkDisable : kGray16
    property color midActive : kGray13
    property color midInactive : kGray13
    property color midDisable : kGray14
    property color textActive : kFontPrimary
    property color textInactive : kFontPrimary
    property color textDisable : kFontPrimaryDisable
    property color brightTextActive : kFontStrong
    property color brightTextInactive : kFontStrong
    property color brightTextDisable : kFontStrong
    property color buttonTextActive : kFontPrimary
    property color buttonTextInactive : kFontPrimary
    property color buttonTextDisable : kFontPrimaryDisable
    property color baseActive : kGray0
    property color baseInactive : kGray0
    property color baseDisable : kGray1
    property color windowActive : kGray2
    property color windowInactive : kGray2
    property color windowDisable : kGray3
    property color shadowActive : Qt.rgba(0, 0, 0, 0.4)
    property color shadowInactive : Qt.rgba(0, 0, 0, 0.25)
    property color shadowDisable : Qt.rgba(0, 0, 0, 0.25)
    property color highlightActive : kBrandNormal
    property color highlightInactive : kBrandNormal
    property color highlightDisable : kGray4
    property color highlightedTextActive : kFontWhite
    property color highlightedTextInactive : kFontWhite
    property color highlightedTextDisable : kFontPrimaryDisable
    property color linkActive : kBrandNormal
    property color linkInactive : kBrandNormal
    property color linkDisable : kFontPrimaryDisable
    property var linkVisitedActive : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(55/255, 144/255, 250/255, 1)
    }
    property var linkVisitedInactive : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(55/255, 144/255, 250/255, 1)
    }
    property color linkVisitedDisable : kFontPrimaryDisable
    property color alternateBaseActive : kGray2
    property color alternateBaseInactive : kGray2
    property color alternateBaseDisable : kGray2
    property color noRoleActive : kGray1
    property color noRoleInactive : kGray1
    property color noRoleDisable : kGray5
    property color toolTipBaseActive : kGray0
    property color toolTipBaseInactive : kGray0
    property color toolTipBaseDisable : kGray0
    property color toolTipTextActive : kFontPrimary
    property color toolTipTextInactive : kFontPrimary
    property color toolTipTextDisable : kFontPrimary
    property color placeholderTextActive : Qt.rgba(0/255, 0/255, 0/255, 0.5)
    property color placeholderTextInactive : Qt.rgba(0/255, 0/255, 0/255, 0.5)
    property color placeholderTextDisable : Qt.rgba(0/255, 0/255, 0/255, 0.35)

    //中性色
    property color kWhite : Qt.rgba(255/255, 255/255, 255/255, 1)
    property color kBlack : Qt.rgba(0/255, 0/255, 0/255, 1)
    property color kGray0 : Qt.rgba(255/255, 255/255, 255/255, 1)
    property color kGray1 : Qt.rgba(250/255, 250/255, 250/255, 1)
    property color kGray2 : Qt.rgba(246/255, 246/255, 246/255, 1)
    property color kGray3 : Qt.rgba(242/255, 242/255, 242/255, 1)
    property color kGray4 : Qt.rgba(238/255, 238/255, 238/255, 1)
    property color kGray5 : Qt.rgba(235/255, 235/255, 235/255, 1)
    property color kGray6 : Qt.rgba(230/255, 230/255, 230/255, 1)
    property color kGray7 : Qt.rgba(220/255, 220/255, 220/255, 1)
    property color kGray8 : Qt.rgba(210/255, 210/255, 210/255, 1)
    property color kGray9 : Qt.rgba(200/255, 200/255, 200/255, 1)
    property color kGray10 : Qt.rgba(185/255, 185/255, 185/255, 1)
    property color kGray11 : Qt.rgba(175/255, 175/255, 175/255, 1)
    property color kGray12 : Qt.rgba(165/255, 165/255, 165/255, 1)
    property color kGray13 : Qt.rgba(115/255, 115/255, 115/255, 1)
    property color kGray14 : Qt.rgba(102/255, 102/255, 102/255, 1)
    property color kGray15 : Qt.rgba(77/255, 77/255, 77/255, 1)
    property color kGray16 : Qt.rgba(64/255, 64/255, 64/255, 1)
    property color kGray17 : Qt.rgba(38/255, 38/255, 38/255, 1)
    property color kGrayAlpha0 : Qt.rgba(0, 0, 0, 0)
    property color kGrayAlpha1 : Qt.rgba(0, 0, 0, 0.05)
    property color kGrayAlpha2 : Qt.rgba(0, 0, 0, 0.08)
    property color kGrayAlpha3 : Qt.rgba(0, 0, 0, 0.1)
    property color kGrayAlpha4 : Qt.rgba(0, 0, 0, 0.15)
    property color kGrayAlpha5 : Qt.rgba(0, 0, 0, 0.18)
    property color kGrayAlpha6 : Qt.rgba(0, 0, 0, 0.2)
    property color kGrayAlpha7 : Qt.rgba(0, 0, 0, 0.25)
    property color kGrayAlpha8 : Qt.rgba(0, 0, 0, 0.28)
    property color kGrayAlpha9 : Qt.rgba(0, 0, 0, 0.3)
    property color kGrayAlpha10 : Qt.rgba(0, 0, 0, 0.35)
    property color kGrayAlpha11 : Qt.rgba(0, 0, 0, 0.55)
    property color kGrayAlpha12 : Qt.rgba(0, 0, 0, 0.03)
    property color kGrayAlpha13 : Qt.rgba(0, 0, 0, 0.4)

    //文本颜色
    property color kFontStrong : Qt.rgba(0, 0, 0, 1)
    property color kFontPrimary : Qt.rgba(0, 0, 0, 0.85)
    property color kFontPrimaryDisable : Qt.rgba(0, 0, 0, 0.35)
    property color kFontSecondary : Qt.rgba(0, 0, 0, 0.6)
    property color kFontSecondaryDisable : Qt.rgba(0, 0, 0, 0.3)
    property color kFontWhite : Qt.rgba(255/255, 255/255, 255/255, 1)
    property color kFontWhiteDisable : Qt.rgba(255/255, 255/255, 255/255, 0.35)
    property color kFontWhiteSecondary : Qt.rgba(255/255, 255/255, 255/255, 0.65)
    property color kFontWhiteSecondaryDisable : Qt.rgba(255/255, 255/255, 255/255, 0.3)

    //功能色
    property color kBrandNormal : Qt.rgba(55/255, 144/255, 250/255, 1)
    property var kBrandHover : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.05)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.05)
        gradientBackground : Qt.rgba(55/255, 144/255, 250/255, 1)
    }
    property var kBrandClick : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(55/255, 144/255, 250/255, 1)
    }
    property var kBrandFocus : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.3)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.3)
        gradientBackground : Qt.rgba(55/255, 144/255, 250/255, 1)
    }
    property color kSuccessNormal : Qt.rgba(0, 180/255, 42/255, 1)
    property var kSuccessHover : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.05)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.05)
        gradientBackground : Qt.rgba(0/255, 180/255, 42/255, 1)
    }
    property var kSuccessClick : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(0/255, 180/255, 42/255, 1)
    }
    property color kWarningNormal : Qt.rgba(255/255, 125/255, 0, 1)
    property var kWarningHover : GradientColor {
        gradientDeg : 0
        gradientStart: Qt.rgba(243/255, 119/255, 0/255, 1)
        gradientEnd: Qt.rgba(243/255, 119/255, 0/255, 1)
        gradientBackground: Qt.rgba(255/255, 255/255, 255/255, 0)
    }
    property var kWarningClick : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(255/255, 125/255, 0/255, 1)
    }
    property color kErrorNormal : Qt.rgba(243/255, 63/255, 63/255, 1)
    property var kErrorHover : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.05)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.05)
        gradientBackground : Qt.rgba(245/255, 63/255, 63/255, 1)
    }
    property var kErrorClick : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(245/255, 63/255, 63/255, 1)
    }
    property color kBrand1 : Qt.rgba(55/255, 144/255, 250/255, 1)
    property color kBrand2 : Qt.rgba(114/255, 46/255, 209/255, 1)
    property color kBrand3 : Qt.rgba(235/255, 48/255, 150/255, 1)
    property color kBrand4 : Qt.rgba(240/255, 188/255, 54/255, 1)
    property color kBrand5 : Qt.rgba(0, 129/255, 31/255, 1)
    property color kBrand6 : Qt.rgba(223/255, 49/255, 55/255, 1)
    property color kBrand7 : Qt.rgba(241/255, 133/255, 43/255, 1)

    //其他颜色
    property color kContainHover : kGray3

    property color kContainClick : kGray4
    property color kContainGeneralNormal : kGray0
    property color kContainSecondaryNormal : kGray2
    property color kContainSecondaryAlphaNormal : kGrayAlpha12
    property color kContainSecondaryAlphaHover : Qt.rgba(0, 0, 0, 0.05)
    property color kContainSecondaryAlphaClick : Qt.rgba(0, 0, 0, 0.08)
    property color kComponentNormal : kGray6
    property color kComponentHover : kGray7
    property color kComponentClick : kGray10
    property color kComponentDisable : kGray4
    property color kComponentAlphaNormal : kGrayAlpha3
    property color kComponentAlphaHover : kGrayAlpha4
    property color kComponentAlphaClick : kGrayAlpha6
    property var kContainGeneralAlphaHover : GradientColor {
        gradientDeg : 0
        gradientStart: Qt.rgba(238/255, 238/255, 238/255, 0.15)
        gradientEnd: Qt.rgba(238/255, 238/255, 238/255, 0.15)
        gradientBackground: Qt.rgba(255/255, 255/255, 255/255, 0)
    }
    property var kContainGeneralAlphaClick : GradientColor {
        gradientDeg : 0
        gradientStart: Qt.rgba(238/255, 238/255, 238/255, 0.2)
        gradientEnd: Qt.rgba(238/255, 238/255, 238/255, 0.2)
        gradientBackground: Qt.rgba(255/255, 255/255, 255/255, 0)
    }
    property color kComponentAlphaDisable : kGrayAlpha2
    property color kLineWindow : Qt.rgba(0, 0, 0, 0.1)
    property color kLineWindowActive : kGrayAlpha3
    property color kLineNormalAlpha : Qt.rgba(0, 0, 0, 0.1)
    property color kLineDisableAlpha : Qt.rgba(0, 0, 0, 0.05)
    property color kLineComponentNormal : kGrayAlpha0
    property color kLineComponentHover : kGrayAlpha0
    property color kLineComponentClick : kGrayAlpha0
    property color kLineComponentDisable : kGrayAlpha0
    property color kLineBrandNormal : Qt.rgba(55/255, 144/255, 250/255, 0)
    property color kLineBrandHover : Qt.rgba(55/255, 144/255, 250/255, 0)
    property color kLineBrandClick : Qt.rgba(55/255, 144/255, 250/255, 0)
    property color kLineBrandDisable : Qt.rgba(55/255, 144/255, 250/255, 0)
    property color kModalMask : Qt.rgba(0, 0, 0, 0.2)

    //2025/01/02根据主题补充token
    property color kSuccessDisable: buttonDisable
    property color kErrorDisable: buttonDisable
    property color kWarningDisable: buttonDisable
    property color kLineNormal: kGrayAlpha3
    property color tokenColor: Qt.rgba(246/255, 246/255, 246/255, 0.65)
    property color tokenColor1: Qt.rgba(255/255, 255/255, 255/255, 0.65)
    property color kContainGeneralAlphaNormal: kGrayAlpha0
    property color kLineWindowInactive: kGrayAlpha3
    property color kContainAlphaClick: kGrayAlpha2
    property color kContainAlphaHover: kGrayAlpha1
    property color kBrandDisable: buttonDisable
    property color kLineDisable: kGrayAlpha1
    property color kDivider: kGrayAlpha3

    //2025/03/03根据5.0主题新增token,默认值为V11主题颜色
    property color kBrand8: Qt.rgba(6/255, 192/255, 199/255, 1)
    property color kLineSelectboxNormal: Qt.rgba(0/255, 0/255, 0/255, 0.2)
    property color kLineSelectboxHover: Qt.rgba(0/255, 0/255, 0/255, 0.24)
    property color kLineSelectboxClick: Qt.rgba(0/255, 0/255, 0/255, 0.28)
    property color kLineSelectboxSelected: Qt.rgba(0/255, 0/255, 0/255, 0)
    property color kLineSelectboxDisable: Qt.rgba(0/255, 0/255, 0/255, 0.08)
    property color kFontWhitePlaceholderTextDisable: Qt.rgba(255/255, 255/255, 255/255, 0.13)
    property color kContainGeneralInactive: kGray1
    property color kFontPlaceholderTextDisable: Qt.rgba(0/255, 0/255, 0/255, 0.12)
    property color kFontWhitePlaceholderText: Qt.rgba(255/255, 255/255, 255/255, 0.35)
    property color kLineInputDisable: Qt.rgba(0/255, 0/255, 0/255, 0.04)
    property color kLineDock: Qt.rgba(255/255, 255/255, 255/255, 0.55)
    property color kLineMenu: Qt.rgba(255/255, 255/255, 255/255, 0.7)
    property color kLineTable: Qt.rgba(0/255, 0/255, 0/255, 0.12)
    property color kLineInputNormal: Qt.rgba(0/255, 0/255, 0/255, 0.08)
    property color kLineInputHover: Qt.rgba(0/255, 0/255, 0/255, 0.12)
    property color kLineInputClick: kBrand1
    property color kOrange1: Qt.rgba(255/255, 128/255, 31/255, 1)
    property color kGreen1: Qt.rgba(24/255, 168/255, 13/255, 1)
    property color kRed1: Qt.rgba(235/255, 70/255, 70/255, 1)
    property color kDividerWhite: Qt.rgba(255/255, 255/255, 255/255, 0.12)

    property color kLineSuccessNormal: Qt.rgba(24/255, 168/255, 13/255, 0.55)
    property var kLineSuccessHover : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.1)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.1)
        gradientBackground : Qt.rgba(24/255, 168/255, 13/255, 0.55)
    }
    property var kLineSuccessClick : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(24/255, 168/255, 13/255, 0.55)
    }
    property color kLineSuccessDisable: Qt.rgba(24/255, 168/255, 13/255, 0.23)
    property color kLineErrorNormal: Qt.rgba(245/255, 73/255, 73/255, 0.55)
    property var kLineErrorHover : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.1)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.1)
        gradientBackground : Qt.rgba(245/255, 73/255, 73/255, 0.55)
    }
    property var kLineErrorClick : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(245/255, 73/255, 73/255, 0.55)
    }
    property color kLineWarningNormal: Qt.rgba(255/255, 128/255, 31/255, 0.55)
    property var kLineWarningHover : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.1)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.1)
        gradientBackground : Qt.rgba(255/255, 128/255, 31/255, 0.55)
    }
    property var kLineWarningClick : GradientColor {
        gradientDeg : 0
        gradientStart : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientEnd : Qt.rgba(0/255, 0/255, 0/255, 0.2)
        gradientBackground : Qt.rgba(255/255, 128/255, 31/255, 0.55)
    }
    property color kLineWarningDisable: Qt.rgba(255/255, 128/255, 31/255, 0.23)

    //描边
    property int normalLine : 1
    property int focusLine : 2

    //圆角
    property int kRadiusMin : 4
    property int kRadiusNormal : 6
    property int kRadiusMax : 8
    property int kRadiusMenu : 8
    property int kRadiusWindow : 12

    //间距
    property int kMarginMin : 4
    property int kMarginNormal : 8
    property int kMarginBig : 16
    property int kMarginWindow : 24
    property int kMarginComponent : 40

    //边距
    property int kPaddingMinLeft : 8
    property int kPaddingMinTop : 2
    property int kPaddingMinRight : 8
    property int kPaddingMinBottom : 2
    property int kPaddingNormalLeft : 16
    property int kPaddingNormalTop : 2
    property int kPaddingNormalRight : 16
    property int kPaddingNormalBottom : 2
    property int kPadding8Left : 8
    property int kPadding8Top : 8
    property int kPadding8Right : 8
    property int kPadding8Bottom : 8
    property int kPaddingWindowLeft : 24
    property int kPaddingWindowTop : 16
    property int kPaddingWindowRight : 24
    property int kPaddingWindowBottom : 24
}

/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: hxf <hewenfei@kylinos.cn>
 *
 */

#include "ukui-quick-platform-plugin.h"
#include "window-type.h"
#include "windows/ukui-window.h"
#include "ukui/ukui-theme-proxy.h"
#include "ukui/settings.h"
#include "ukui/app-launcher.h"
#include "ukui/dt-theme-definition.h"
#include "window-manager.h"
#include "ukui/dt-theme.h"
#include <QMetaType>

void UkuiQuickPlatformPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QString(uri) == QLatin1String(PLUGIN_IMPORT_URI));
    qmlRegisterModule(uri, PLUGIN_VERSION_MAJOR, PLUGIN_VERSION_MINOR);
    qmlRegisterSingletonType<UkuiQuick::Settings>(uri, 1, 0, "Settings", [] (QQmlEngine *, QJSEngine *) -> QObject* {
        return UkuiQuick::Settings::instance();
    });
    qmlRegisterSingletonType<UkuiQuick::AppLauncher>(uri, 1, 0, "AppLauncher", [] (QQmlEngine *, QJSEngine *) -> QObject* {
        return UkuiQuick::AppLauncher::instance();
    });
    qmlRegisterUncreatableType<UkuiQuick::Theme>(uri, PLUGIN_VERSION_MAJOR, PLUGIN_VERSION_MINOR, "Theme", "Accessing Theme through Attached Property.");
    qmlRegisterUncreatableType<UkuiQuick::WindowType>(uri, PLUGIN_VERSION_MAJOR, PLUGIN_VERSION_MINOR, "WindowType",
                                                      "WindowType is a read-only interface used to access enumeration properties.");

    qmlRegisterExtendedType<UkuiQuick::UKUIWindow, UkuiQuick::UKUIWindowExtension>(uri, 1, 0, "UKUIWindow");
    qmlRegisterSingletonType<UkuiQuick::WindowManager>(uri, 1, 0, "WindowManager", [] (QQmlEngine *, QJSEngine *) -> QObject* {
    return UkuiQuick::WindowManager::self();
    });
    qmlRegisterType<UkuiQuick::DtThemeDefinition>(uri, 1, 0, "DtThemeDefinition");
    qmlRegisterSingletonType<UkuiQuick::DtTheme>(uri, 1, 0, "GlobalTheme", [] (QQmlEngine *engine, QJSEngine *) -> QObject* {
        return new UkuiQuick::DtTheme(engine);
    });
    qRegisterMetaType<UkuiQuick::GradientColor>("GradientColor");
    qmlRegisterType<UkuiQuick::GradientColor>(uri, 1, 0, "GradientColor");
    qmlRegisterUncreatableType<UkuiQuick::DtColorType>(uri, 1, 0, "DtColorType", "DtColorType is a read-only interface used to access enumeration properties.");
    qRegisterMetaType<UkuiQuick::DtColorType>("DtColorType");
}

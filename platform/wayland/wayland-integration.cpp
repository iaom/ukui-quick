/*
    SPDX-FileCopyrightText: 2020 Vlad Zahorodnii <vlad.zahorodnii@kde.org>

    Based on WaylandIntegration from kwayland-integration

    SPDX-FileCopyrightText: 2014 Martin Gräßlin <mgraesslin@kde.org>
    SPDX-FileCopyrightText: 2015 Marco Martin <mart@kde.org>
    SPDX-FileCopyrightText: 2024 iaom <zhangpengfei@kylinos.cn>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "wayland-integration_p.h"
#include <KWayland/Client/connection_thread.h>
#include <windowmanager/ukuishell.h>
#include "registry.h"

#include <QCoreApplication>
#include <QDebug>

class WaylandIntegrationSingleton
{
public:
    WaylandIntegration self;
};

Q_GLOBAL_STATIC(WaylandIntegrationSingleton, privateWaylandIntegrationSelf)

WaylandIntegration::WaylandIntegration(QObject *parent)
    : QObject(parent)
{
    setupKWaylandIntegration();
}

WaylandIntegration::~WaylandIntegration()
{
}

UkuiShell *WaylandIntegration::waylandUkuiShell()
{
    if (!m_waylandUkuiShell && m_registry) {
        const UkuiQuick::WaylandClient::Registry::AnnouncedInterface interface = m_registry->interface(UkuiQuick::WaylandClient::Registry::Interface::UkuiShell);

        if (interface.name == 0) {
            qWarning() << "The compositor does not support the ukui shell protocol";
            return nullptr;
        }

        m_waylandUkuiShell = m_registry->createUkuiShell(interface.name, interface.version, qApp);

        connect(m_waylandUkuiShell, &UkuiShell::removed, this, [this]() {
            m_waylandUkuiShell->deleteLater();
        });
    }

    return m_waylandUkuiShell;
}

WaylandIntegration *WaylandIntegration::self()
{
    return &privateWaylandIntegrationSelf()->self;
}

void WaylandIntegration::sync()
{
    wl_display_roundtrip(m_connection->display());
}

void WaylandIntegration::setupKWaylandIntegration()
{
    m_connection = KWayland::Client::ConnectionThread::fromApplication(this);
    if (!m_connection) {
        return;
    }

    m_registry = new UkuiQuick::WaylandClient::Registry(qApp);
    m_registry->create(m_connection);
    m_registry->setup();
    m_connection->roundtrip();
}
